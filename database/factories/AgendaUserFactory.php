<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\AgendaUser;
use App\Position;
use App\Agenda;
use App\User;
use Faker\Generator as Faker;

$factory->define(AgendaUser::class, function (Faker $faker) {
    $agenda = Agenda::pluck('id')->random();
    $user = User::pluck('id')->random();
    $position = Position::pluck('id')->random();
    return [
        'agenda_id' => $agenda,
        'user_id' => $user,
        'position_id' => $position,
    ];
});
