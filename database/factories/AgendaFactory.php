<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Agenda;
use Faker\Generator as Faker;

$factory->define(Agenda::class, function (Faker $faker) {
    $gender = $faker->randomElement(['male', 'female']);
    return [
        'name' => $faker->name($gender),
        'description' => '',
        'destination' => $faker->address,
        'start_time' => $faker->date('Y-m-d', '2004-01-01'),
        'end_time' => $faker->date('Y-m-d', '2004-01-02'),
    ];
        // 'date_of_birth' => $faker->dateTimeBetween('1970-01-01', '2000-12-30')
    				// 		->format('d/m/Y'),
});
