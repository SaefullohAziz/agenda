<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\User::create([
            'username' => 'admin',
            'name' => 'Admin',
            'email' => 'admin@agenda.com',
            'password' => \Illuminate\Support\Facades\Hash::make('sysAdmin'),
        ]);
        \App\User::create([
            'username' => 'Ahmad',
            'name' => 'Ahmad',
            'email' => 'Ahmad@agenda.com',
            'password' => \Illuminate\Support\Facades\Hash::make('sysAdmin'),
        ]);
        \App\User::create([
            'username' => 'Azrial',
            'name' => 'Azrial Fatur',
            'email' => 'Azrial@agenda.com',
            'password' => \Illuminate\Support\Facades\Hash::make('sysAdmin'),
        ]);
        \App\User::create([
            'username' => 'Aziz',
            'name' => 'Saefulloh Aziz',
            'email' => 'Aziz@agenda.com',
            'password' => \Illuminate\Support\Facades\Hash::make('sysAdmin'),
        ]);
    }
}
