<?php

use Illuminate\Database\Seeder;

class PositionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = ['Leader', 'Singer', 'Pemusik', 'Pendoa', 'Konselor', 'Pewarta', 'Team_musik'];
        foreach ($data as $record) {
            \App\Position::firstOrCreate(['name' => $record]);
        }
    }
}
