<?php

use Illuminate\Database\Seeder;

class DummyDatasSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $agendas = factory(App\Agenda::class, 5)->create();
        $agendaUsers = factory(App\AgendaUser::class, 5)->create();
    }
}
