<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\Traits\Uuids;

class User extends Authenticatable
{
    use Uuids, Notifiable;

    public function agendaUsers() {
        return $this->hasMany('App\AgendaUser');
    }

    public function agendas() {
        return $this->belongsToMany('App\Agenda', 'agenda_users')->using('App\AgendaUser')->withTimestamps()->withPivot('created_at')->orderBy('agendas.created_at', 'asc');
    }

    public function position() {
        return $this->belongsToMany('App\position', 'agenda_users')->using('App\AgendaUser');
    }

    public function log() {
        return $this->hasMany('App\Log', 'user_id', 'id');
    }

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'username', 'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    /**
     * Get the user's avatar.
     *
     * @param  string  $value
     * @return string
     */
    public function getAvatarAttribute()
    {
        if ($this->photo == 'avatar.png') {
            return '/img/avatar/avatar.png';
        }
        return '/storage/avatar/' . $this->photo;
    }
}
