<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\Pivot;
use App\Traits\Uuids;

class AgendaUser extends Pivot
{
    use Uuids;

    protected $table = 'agenda_users';
    protected $fillable = ['agenda_id', 'user_id', 'position_id'];

    public function user() {
        return $this->hasOne('App\User', 'id', 'user_id');
    }

    public function position() {
        return $this->hasOne('App\Position', 'id', 'position_id');
    }
}
