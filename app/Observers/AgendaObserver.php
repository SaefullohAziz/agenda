<?php

namespace App\Observers;
use app\Agenda;

class AgendaObserver
{
    /**
     * Handle the  agenda "created" event.
     *
     * @param  \App\Agenda  $=Agenda
     * @return void
     */
    public function created(Agenda $agenda)
    {
        $agenda->log()->create(['user_id' => Auth()->user()->id, 'description' => 'Created by ']);
    }

    /**
     * Handle the  agenda "updated" event.
     *
     * @param  \App\Agenda  $agenda
     * @return void
     */
    public function updated(Agenda $agenda)
    {
        $agenda->log()->create(['user_id' => Auth()->user()->id, 'description' => 'Edited by ']);
    }

    /**
     * Handle the  agenda "deleted" event.
     *
     * @param  \App\Agenda  $agenda
     * @return void
     */
    public function deleted(Agenda $agenda)
    {
        $agenda->log()->create(['user_id' => Auth()->user()->id, 'description' => 'Deleted by ']);
    }

    /**
     * Handle the  agenda "restored" event.
     *
     * @param  \App\Agenda  $agenda
     * @return void
     */
    public function restored(Agenda $agenda)
    {
        //
    }

    /**
     * Handle the  agenda "force deleted" event.
     *
     * @param  \App\Agenda  $agenda
     * @return void
     */
    public function forceDeleted(Agenda $agenda)
    {
        //
    }
}
