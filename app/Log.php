<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Traits\Uuids;

class Log extends Model
{
	use Uuids;
	protected $fillable = ['agenda_id', 'user_id', 'description'];

    public function agenda() {
        return $this->belongsTo('App\Agenda');
    }

    public function user() {
        return $this->belongsTo('App\User', 'user_id');
    }
}
