<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Traits\Uuids;

class Position extends Model
{
    use Uuids;

    public function agendaUser() {
        return $this->belongsTo('App\Position');
    }
}
