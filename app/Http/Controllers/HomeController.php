<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Agenda;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */


    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        // $plus = mktime(0,0,0,date("n"),date("j")+7,date("Y"));
        // $tambah = date("Y-m-d", $plus);
        // $now = date('Y-m-d');
        // $week = Agenda::where([
        //     ['start_time', '>=' , $now],
        //     ['start_time', '<=' , $tambah],
        // ])->get();

        // $events = [];
        // $agendas = Agenda::where([
        //     ['start_time', '>=' , date('Y') - 1 . '-01-01'],
        //     ['start_time', '<=' , date('Y') + 1 . '-01-01'],
        // ])->get();

        // foreach ($agendas as $agenda) {
        //     $events[] = \Calendar::event(
        //         $agenda->name, //event title
        //         false, //full day event?
        //         $agenda->start_time, //start time (you can also use Carbon instead of DateTime)
        //         $agenda->end_time, //end time (you can also use Carbon instead of DateTime)
        //         $agenda->id //optionally, you can specify an event ID
        //     );
        // }

        // $calendar = \Calendar::addEvents($events, [ //set custom color fo this event
        //     'color' => '#63ED7A', 
        // ])->setOptions([ //set fullcalendar options
        //     'firstDay' => 1,
        //     'defaultView' => 'agendaWeek',
        // ]);

        $tasks = Agenda::all();
        $view = [
            'title' => 'home',
            'tasks' => $tasks
        ];
        
        return view('home', $view);
    }
}
