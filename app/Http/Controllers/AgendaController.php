<?php

namespace App\Http\Controllers;

use App\User;
use App\Agenda;
use App\position;
use App\AgendaUser;
use DataTables;
use Illuminate\Http\Request;

class AgendaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $view = [
            'title' => 'Agenda Kegiatan',
        ];

        return view('agenda.index', $view);
    }

    /**
     * Show a listing of the resource for datatable.
     * 
     * @param  \Illuminate\Http\Request  $request
     */
    public function list(Request $request)
    {
        if ($request->ajax()) {
            $agenda = Agenda::get();
            return DataTables::of($agenda)
                ->addColumn('DT_RowIndex', function ($data) {
                    return '<div class="checkbox icheck"><label><input type="checkbox" name="selectedData[]" value="'.$data->id.'"></label></div>';
                })
                ->editColumn('created_at', function($data) {
                    return (date('d-m-Y H:i:s', strtotime($data->created_at)));
                })
                ->editColumn('name', function($data) {
                    $explode = explode('\n', $data->name);
                    return implode(', ',$explode);
                })
                ->editColumn('start_time', function($data) {
                    return (date('l, F, d H:i:s', strtotime($data->start_time)));
                })
                ->editColumn('end_time', function($data) {
                    return (date('l, F, d H:i:s', strtotime($data->end_time)));
                })
                ->addColumn('action', function($data) {
                    return '<a class="btn btn-sm btn-info" href="'.route('agenda.show', $data->id).'" title="'.__('See detail').'"><i class="fa fa-eye"></i> '.__('See').'</a> <a class="btn btn-sm btn-warning" href="'.route('agenda.edit', $data->id).'" title="'.__('Edit').'"><i class="fa fa-edit"></i> '.__('Edit').'</a>';
                })
                ->rawColumns(['DT_RowIndex', 'action'])
                ->make(true);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $view = [
            'title' => 'Create',
            'type' => [
                'pujian' => 'Pujian', 
                'doa' => 'Doa dan konselor', 
                'pewarta' => 'Pewarta & Pengajar'
            ],
            'users' => User::pluck('name', 'name')->toArray(),
        ];

        return view('agenda.create', $view);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if ($request->end_time) {
            $this->validate($request, [
                'name' => 'required',
                'start_time' => 'required',
                'end_time' => 'after_or_equal:start_time'
            ]); 
        }else{
            $this->validate($request, [
                'name' => 'required',
                'start_time' => 'required',
            ]); 
        }
        $name = '';
        if (!empty($request->name)) { $name .= ' ' . $request->name . ' \n '; }
        if (!empty($request->pembicara)) { $name .= 'Pembicara : ' . $request->pembicara . ' \n '; }
        if (!empty($request->leader)) { $name .= 'WL : ' . implode(', ', $request->leader) . ' \n '; }
        if (!empty($request->pemusik)) { $name .= 'Pemusik : ' . implode(', ', $request->pemusik) . ' \n '; }
        if (!empty($request->mulmed)) { $name .= 'Mulmed : ' . implode(', ', $request->mulmed) . ' \n '; }
        if (!empty($request->singer)) { $name .= 'Singer : ' . implode(', ', $request->singer) . ' \n '; }
        if (!empty($request->pewarta)) { $name .= 'Pewarta : ' . $request->pewarta . ' \n '; }

        $description = '';
        if (!empty($request->tema)) { $description .= ' Tema : ' . $request->tema . ' \n '; }
        if (!empty($request->pendoa)) { $description .= 'Konselor / pendoa : ' . $request->pendoa . ' \n '; }
        if (!empty($request->kegiatan)) { $description .= 'Kegiatan : ' . $request->kegiatan . ' \n '; }
        if (!empty($request->konseliName)) { $description .= 'Konseli / yang didoakan : ' . $request->konseliName . ' \n '; }
        $request->merge([
            'name' => $name,
            'description' => $description . $request->description,
        ]);

        $agenda = Agenda::create($request->all());

        // if($agenda) {
        //     foreach ($request->position as $position => $value) {
        //         $position_id = Position::where('name' , $position)->select('id')->first()->id;
        //         if(is_array($value)) {
        //             foreach ($value as $user_id) {
        //                 if ($user_id) {
        //                     $agenda->agendaUsers()->create([
        //                         'user_id' => $user_id,
        //                         'position_id' => $position_id
        //                     ]);
        //                 }
        //             }
        //         }else{
        //             if ($value) {
        //                 $agenda->agendaUsers()->create([
        //                     'user_id' => $value,
        //                     'position_id' => $position_id
        //                 ]);
        //             }
        //         }
        //     }
        // }

        return redirect()->route('home')->with('alert-success', 'Create success!');        
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Agenda  $agenda
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $pembicara = '';
        $wl = '';
        $pemusik = '';
        $mulmed = '';
        $singer = '';
        $pewarta = '';
        $agenda = Agenda::find($id);
        $explode = explode('\n', $agenda->name);
        $kegiatan = $explode[0];
        foreach ($explode as $key) {
            preg_match('/Pembicara/i', $key) ? $pembicara = $key : '';
            preg_match('/WL/i', $key) ? $wl = $key : '';
            preg_match('/Pemusik/i', $key) ? $pemusik = $key : '';
            preg_match('/Mulmed/i', $key) ? $mulmed = $key : '';
            preg_match('/Singer/i', $key) ? $singer = $key : '';
            preg_match('/Pewarta/i', $key) ? $pewarta = $key : '';
        }
        $view = [
            'title' => 'Agenda',
            'agenda' => $agenda,
            'kegiatan' => $kegiatan,
            'pembicara' => $pembicara,
            'wl' => $wl,
            'pemusik' => $pemusik,
            'mulmed' => $mulmed,
            'singer' => $singer,
            'pewarta' => $pewarta,
        ];

        return view('agenda.show', $view);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Agenda  $agenda
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $pembicara = '';
        $wl = '';
        $pemusik = '';
        $mulmed = '';
        $singer = '';
        $pewarta = '';
        $agenda = Agenda::find($id);
        $explode = explode('\n', $agenda->name);
        $kegiatan = $explode[0];
        foreach ($explode as $key) {
            preg_match('/Pembicara/i', $key) ? $pembicara = $key : '';
            preg_match('/WL/i', $key) ? $wl = $key : '';
            preg_match('/Pemusik/i', $key) ? $pemusik = $key : '';
            preg_match('/Mulmed/i', $key) ? $mulmed = $key : '';
            preg_match('/Singer/i', $key) ? $singer = $key : '';
            preg_match('/Pewarta/i', $key) ? $pewarta = $key : '';
        }
        $view = [
            'title' => 'Agenda',
            'agenda' => $agenda,
            'kegiatan' => $kegiatan,
            'pembicara' => $pembicara,
            'wl' => $wl,
            'pemusik' => $pemusik,
            'mulmed' => $mulmed,
            'singer' => $singer,
            'pewarta' => $pewarta,
        ];

        return view('agenda.edit', $view);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Agenda  $agenda
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Agenda $agenda)
    {
        $name = '';
        if (!empty($request->name)) { $name .= ' ' . $request->name . ' \n '; }
        if (!empty($request->pembicara)) { $name .= 'Pembicara : ' . $request->pembicara . ' \n '; }
        if (!empty($request->leader)) { $name .= 'WL : ' . $request->leader . ' \n '; }
        if (!empty($request->pemusik)) { $name .= 'Pemusik : ' . $request->pemusik . ' \n '; }
        if (!empty($request->mulmed)) { $name .= 'Mulmed : ' . $request->mulmed . ' \n '; }
        if (!empty($request->singer)) { $name .= 'Singer : ' . $request->singer . ' \n '; }
        if (!empty($request->pewarta)) { $name .= 'Pewarta : ' . $request->pewarta . ' \n '; }

        $request->merge([
            'name' => $name,
        ]);

        $agenda->fill($request->all());

        $agenda->save();
        return redirect()->route('agenda.index')->with('alert-success', 'Agenda has been changed!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Agenda  $agenda
     * @return \Illuminate\Http\Response
     */
    public function destroy(Agenda $agenda)
    {
        //
    }
}
