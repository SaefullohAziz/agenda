<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Traits\Uuids;
use App\AgendaUserPosition;
use App\AgendaUser;
use App\User;

class Agenda extends Model implements \MaddHatter\LaravelFullcalendar\IdentifiableEvent
{
    use Uuids;

    protected $fillable = ['name', 'description', 'destination', 'start_time', 'end_time'];
    protected $dates = ['start_time', 'end_time', 'created_at', 'updated_at'];

    public function agendaUsers() {
        return $this->hasMany('App\AgendaUser', 'agenda_id');
    }

    public function users() {
        return $this->belongsToMany('App\User', 'agenda_users')->using('App\AgendaUser')->withTimestamps()->withPivot('created_at')->orderBy('users.name', 'asc');
    }

    public function log() {
        return $this->hasMany('App\Log', 'agenda_id', 'id');
    }

    /**
     * Get the event's id number
     *
     * @return int
     */
    public function getId() {
		return $this->id;
	}

    /**
     * Get the event's title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Is it an all day event?
     *
     * @return bool
     */
    public function isAllDay()
    {
        return (bool)$this->all_day;
    }

    /**
     * Get the start time
     *
     * @return DateTime
     */
    public function getStart()
    {
        return $this->start_time;
    }

    /**
     * Get the end time
     *
     * @return DateTime
     */
    public function getEnd()
    {
        return $this->end_time;
    }
}
