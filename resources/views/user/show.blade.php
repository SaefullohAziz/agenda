@extends('layouts.main')

@section('content')
<div class="row mt-sm-4">
    <div class="col-12 col-md-12 col-lg-7">

        @if (session('alert-success'))
            <div class="alert alert-success alert-dismissible show fade">
                <div class="alert-body">
                    <button class="close" data-dismiss="alert">
                        <span>&times;</span>
                    </button>
                    {{ session('alert-success') }}
                </div>
            </div>
        @endif

        @if (session('alert-danger'))
            <div class="alert alert-danger alert-dismissible show fade">
                <div class="alert-body">
                    <button class="close" data-dismiss="alert">
                        <span>&times;</span>
                    </button>
                    {{ session('alert-danger') }}
                </div>
            </div>
        @endif

        <div class="card">
            <div class="card-header">
                <h4>{{ __('See Profile') }}</h4>
            </div>
            <div class="card-body">
                <div class="row">
                    {{ Form::bsText('col-sm-6', __('Username'), 'username', $data->username, __('Username'), ['disabled' => '']) }}

                    {{ Form::bsText('col-sm-6', __('Name'), 'name', $data->name, __('Name'), ['disabled' => '']) }}

                    {{ Form::bsEmail('col-sm-6', __('E-Mail'), 'email', $data->email, __('E-Mail'), ['disabled' => '']) }}
                </div>
            </div>
        </div>

    </div>
</div>
@endsection