@extends('layouts.main')
@section('content')
        <div class="card">
            <div class="card-header">
                <h4>{{ __('Tambah Agenda') }}</h4>
            </div>
            <div class="card-body">
                <div class="row">
                    {{ Form::bsSelect('col-sm-4', null, 'Jenis Agenda', $form, null, __('Pilih Jenis Agenda')) }}
                </div>
            </div>
            <div class="card-footer text-right">
            </div>
        </div>
@endsection

@section('script')
@endsection