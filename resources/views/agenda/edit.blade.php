@extends('layouts.main')

@section('content')
<div class="row">
    <div class="col-12">

        @if (session('alert-success'))
            <div class="alert alert-success alert-dismissible show fade">
                <div class="alert-body">
                    <button class="close" data-dismiss="alert">
                        <span>&times;</span>
                    </button>
                    {{ session('alert-success') }}
                </div>
            </div>
        @endif

        @if (session('alert-danger'))
            <div class="alert alert-danger alert-dismissible show fade">
                <div class="alert-body">
                    <button class="close" data-dismiss="alert">
                        <span>&times;</span>
                    </button>
                    {{ session('alert-danger') }}
                </div>
            </div>
        @endif

        <div class="card card-primary">

            {{ Form::open(['route' => ['agenda.update', $agenda->id], 'method' => 'put', 'files' => true]) }}
                <div class="card-body">
                    <div class="row">

                        {{ Form::bsText('col-sm-6', __('Kegiatan'), 'name', $kegiatan, __('Name'), ['required' => '']) }}

                        {{ Form::bsText('col-sm-6', __('Destination'), 'destination', $agenda->destination, __('destination'), ['required' => '']) }}


                        {{ Form::bsText('col-md-6', 'Tanggal pelayanan', 'start_time', $agenda->start_time, __('datetime')) }}

                        {{ Form::bsText('col-md-6', 'Sampai', 'end_time', $agenda->end_time, __('datetime')) }}

                        {{ Form::bsText('col-md-6', 'Pembicara', 'pembicara', $pembicara, __('Ex : Dimas / Dimas & Budi / Dimas, Budi, & Aji'), ['' => '']) }}

                        {{ Form::bsText('col-md-6', 'Worship Leader', 'leader', $wl, __('Ex : Dimas / Dimas & Budi / Dimas, Budi, & Aji'), ['' => '']) }}

                        {{ Form::bsText('col-md-6', 'Pemusik', 'pemusik', $pemusik, __('Ex : Dimas / Dimas & Budi / Dimas, Budi, & Aji'), ['' => '']) }}

                        {{ Form::bsText('col-md-6', 'Mulmed', 'mulmed', $mulmed, __('Ex : Dimas / Dimas & Budi / Dimas, Budi, & Aji'), ['' => '']) }}


                        {{ Form::bsText('col-md-12', 'Singer', 'singer', $singer, __('Ex : Dimas / Dimas & Budi / Dimas, Budi, & Aji'), ['' => '']) }}
                    </div>
                </div>
                <div class="card-footer bg-whitesmoke text-center">
                    {{ Form::submit(__('Save'), ['name' => 'submit', 'class' => 'btn btn-primary']) }}
                    {{ link_to(url()->previous(), __('Cancel'), ['class' => 'btn btn-danger']) }}
                </div>
            {{ Form::close() }}

        </div>
    </div>
</div>
@endsection

@section('script')
<script>
    $(document).ready(function () { 
        $('[name="start_time"], [name="end_time"]').daterangepicker({
            locale: {format: 'YYYY-MM-DD HH:mm:ss'},
            singleDatePicker: true,
            timePicker: true,
            timePicker24Hour: true,
        });
    });
</script>
@endsection
