@extends('layouts.main')
@section('content')

        @if (session('alert-success'))
            <div class="alert alert-success alert-dismissible show fade">
                <div class="alert-body">
                    <button class="close" data-dismiss="alert">
                        <span>&times;</span>
                    </button>
                    {{ session('alert-success') }}
                </div>
            </div>
        @endif

        @if (session('alert-danger'))
            <div class="alert alert-danger alert-dismissible show fade">
                <div class="alert-body">
                    <button class="close" data-dismiss="alert">
                        <span>&times;</span>
                    </button>
                    {{ session('alert-danger') }}
                </div>
            </div>
        @endif

        <div class="card">
            <div class="card-header">
                <h4>{{ __('Tambah Agenda') }}</h4>
            </div>
            {{ Form::open(['route' => 'agenda.store', 'files' => true]) }}
            <div class="card-body">
                <div class="row">
                    <div class="col-sm-6 col-lg-3">
                    </div>
                    <div class="col-sm-6 col-lg-9">
                        <label style="color: red">*Kosongkan field yang tidak di perlukan</label>
                    </div>
                    <div class="col-sm-6 col-lg-3">
                        {{ Form::bsSelect('', 'Jenis Kegiatan', 'type', $type, null, __('Pilih Jenis Kegiatan')) }}
                    </div>
                    <div class="col-sm-6 col-lg-9">
                        {{ Form::bsText(null, 'Nama Kegiatan', 'name', old('nama agenda'), __('Nama Agenda')) }}

                        {{ Form::bsText(null, 'Tujuan acara', 'destination', (empty(old('destination'))?'PDPKK ST. Laurentius Bandung':old('destination')), __('Destination')) }}


                        <div class="row">
                            {{ Form::bsText('col-md-6', 'Waktu', 'start_time', old('start_time'), __('datetime')) }}
                            <div class="col-md-1">
                                End <input type="radio" name="radio" id="radio">
                            </div>
                            <div class="col-md-5 radio-check d-none">                                {{ Form::bsText(null, ' ', 'end_time', old('end_time'), __('datetime')) }}
                            </div>
                        </div>

                        <div class="pujian d-block">
                            <div class="row">
                                {{ Form::bsText('col-md-6', 'Pembicara', 'pembicara', old('Pembicara'), __('Ex : Dimas'), ['' => '']) }}

                                {{ Form::bsSelect('col-md-6', 'Worship Leader', 'leader[]', $users, null, __('Select WL'), ['Multiple' => '']) }}

                                {{ Form::bsSelect('col-md-6', 'Pemusik', 'pemusik[]', $users, null, __('Select Pemusik'), ['Multiple' => '']) }}

                                {{ Form::bsSelect('col-md-6', 'Mulmed', 'mulmed[]', $users, null, __('Select Mulmed'), ['Multiple' => '']) }}


                                {{ Form::bsSelect('col-md-12', 'Singer', 'singer[]', $users, null, __('Select Singer'), ['Multiple' => '']) }}
                            </div>
                        </div>

                        <div class="pewarta d-none">
                            {{ Form::bsText(null, 'Pewarta', 'pewarta', old('pewarta'), __('Ex : Dimas / Dimas & Budi / Dimas, Budi, & Aji'), ['' => '']) }}

                            {{ Form::bsText(null, 'Tema', 'tema', old('tema'), __('tema pewartaan/ bahan pengajaran')) }}
                            
                        </div>

                        <div class="doa d-none">

                            {{ Form::bsText(null, 'Nama pendoa / konselor', 'pendoa', old('pendoa'), __('Nama Pendoa')) }}

                            {{ Form::bsSelect(null, __('Kegiatan'), 'kegiatan', ['Mendoakan' => 'Mendoakan', 'Konseling' => 'Konseling', 'Konseling dan Mendoakan' => 'Konseling dan Mendoakan'], null, __('Pilih kegiatan'), ['placeholder' => 'Pilih Kegiatan']) }}
                            
                            {{ Form::bsText(null, 'Nama konseli / yang didoakan', 'konseliName', old('konseliName'), __('Nama konseli / yang didoakan')) }}
                            
                        </div>
                        {{ Form::bsTextarea(null, __('Deskripsi tambahan (optional)/ keterangan'), 'description', old('description'), __('Description')) }}
                    </div>
                </div>
            </div>
            <div class="card-footer text-center">
                {{ Form::submit(__('Save'), ['name' => 'submit', 'class' => 'btn btn-primary']) }}
                {{ link_to(route('agenda.index'), __('Cancel'), ['class' => 'btn btn-danger']) }}
            </div>
            {{ Form::close() }}
        </div>
@endsection

@section('script')
<script>
    $(document).ready(function () {
        $('.pewarta select').append('<option value="" selected="true">Choose Pewarta</option>');
        $('[name="type"]').change(function(){
            $('.doa, .pewarta, .pujian').addClass('d-none').removeClass('d-block');
            $('.' + $(this).val()).addClass('d-block').removeClass('d-none');
        })
        $('[name="start_time"]').daterangepicker({
        	locale: {format: 'YYYY-MM-DD HH:mm:ss'},
        	singleDatePicker: true,
            timePicker: true,
            timePicker24Hour: true,
      	});
        $('input[name="radio"]').change(function(e) { 
            $('.radio-check').addClass('d-block').removeClass('d-none');
            $('[name="end_time"]').daterangepicker({
                locale: {format: 'YYYY-MM-DD HH:mm:ss'},
                singleDatePicker: true,
                timePicker: true,
                timePicker24Hour: true,
            });
        });
        $('select[name="leader[]"]').change(function(e) { 
            $('select[name="pemusik[]"] option, select[name="mulmed[]"] option, select[name="singer[]"] option').remove();
            @foreach ($users as $user)
                $('select[name="pemusik[]"], select[name="mulmed[]"], select[name="singer[]"]').append('<option value="{{ $user }}">{{ $user }}</option>');
            @endforeach
            let val = $(this).val();
            val.forEach(function (value) {
                $('select[name="pemusik[]"] option[value="'+ value +'"]').remove();
                $('select[name="mulmed[]"] option[value="'+ value +'"]').remove();
                $('select[name="singer[]"] option[value="'+ value +'"]').remove();
            });
        });
    });

    function allowDrop(ev) {
      ev.preventDefault();
    }

    function drag(ev) {
      ev.dataTransfer.setData("text", ev.target.id);
    }

    function drop(ev) {
      ev.preventDefault();
      var data = ev.dataTransfer.getData("text");
      ev.target.appendChild(document.getElementById(data));
    }
</script>
@endsection