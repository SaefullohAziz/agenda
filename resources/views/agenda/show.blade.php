@extends('layouts.main')

@section('content')
<div class="row">
    <div class="col-12">

        @if (session('alert-success'))
            <div class="alert alert-success alert-dismissible show fade">
                <div class="alert-body">
                    <button class="close" data-dismiss="alert">
                        <span>&times;</span>
                    </button>
                    {{ session('alert-success') }}
                </div>
            </div>
        @endif

        @if (session('alert-danger'))
            <div class="alert alert-danger alert-dismissible show fade">
                <div class="alert-body">
                    <button class="close" data-dismiss="alert">
                        <span>&times;</span>
                    </button>
                    {{ session('alert-danger') }}
                </div>
            </div>
        @endif

        <div class="card card-primary">
            <div class="card-body">
                <div class="row">

                        {{ Form::bsText('col-sm-6', __('Kegiatan'), 'name', $kegiatan, __('Name'), ['readonly' => '']) }}

                        {{ Form::bsText('col-sm-6', __('Destination'), 'destination', $agenda->destination, __('destination'), ['readonly' => '']) }}


                        {{ Form::bsText('col-md-6', 'Tanggal pelayanan', 'start_time', $agenda->start_time, __('datetime'), ['readonly' => '']) }}

                        {{ Form::bsText('col-md-6', 'Sampai', 'end_time', $agenda->end_time, __('datetime'), ['readonly' => '']) }}

                        {{ Form::bsText('col-md-6', 'Pembicara', 'pembicara', $pembicara, __(' '), ['readonly' => '']) }}

                        {{ Form::bsText('col-md-6', 'Worship Leader', 'leader', $wl, __(' '), ['readonly' => '']) }}

                        {{ Form::bsText('col-md-6', 'Pemusik', 'pemusik', $pemusik, __(' '), ['readonly' => '']) }}

                        {{ Form::bsText('col-md-6', 'Mulmed', 'mulmed', $mulmed, __(' '), ['readonly' => '']) }}


                        {{ Form::bsText('col-md-12', 'Singer', 'singer', $singer, __(' '), ['readonly' => '']) }}
                </div>
            </div>
            <div class="card-footer bg-whitesmoke text-center">
                {{ link_to(url()->previous(), __('Cancel'), ['class' => 'btn btn-danger']) }}
            </div>
        </div>
    </div>
</div>
@endsection

@section('script')
@endsection
