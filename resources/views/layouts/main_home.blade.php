@include('layouts.header')

<div id="app">
  <div class="main-wrapper main-wrapper-1">
   
  @include('layouts.navbar_home')

    <!-- Main Content -->
    <div class="container mt-5">
          @yield('content')
    </div>
    <footer class="main-footer">
      <div class="footer-left">
        {{ __('Copyright') }} &copy; 2019
      </div>
      <div class="footer-right">

      </div>
    </footer>
  </div>
</div>

@include('layouts.footer')