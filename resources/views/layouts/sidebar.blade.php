<div class="main-sidebar sidebar-style-2">
  <aside id="sidebar-wrapper">
    <div class="sidebar-brand">
      <a href="{{ route('home') }}">{{ config('app.name', 'Laravel') }}</a>
    </div>
    <div class="sidebar-brand sidebar-brand-sm">
      <a href="{{ route('home') }}">{{ config('app.shortname', 'LV') }}</a>
    </div>
    <ul class="sidebar-menu">
      <li class="menu-header">{{ __('Dashboard') }}</li>
      <li class="{{ (request()->is('/')?'active':'') }}">
        <a class="nav-link" href="{{ route('home') }}">
          <i class="fa fa-home"></i> <span>{{ __('Home') }}</span>
        </a>
      </li>
      @if(Auth::check())
        @if(Auth::user()->name == 'timmy' || Auth::user()->name == 'Admin')
          <li class="menu-header">{{ __('Menu') }}</li>
          <li class="{{ (request()->is('agenda')||request()->is('agenda/*')?'active':'') }}">
            <a class="nav-link" href="{{ route('agenda.index') }}">
              <i class="fa fa-calendar"></i> <span>{{ __('Agendas') }}</span>
            </a>
          </li>
          <li class="{{ (request()->is('user')||request()->is('user/*')?'active':'') }}">
            <a class="nav-link" href="{{ route('user.index') }}">
              <i class="fa fa-user"></i> <span>{{ __('Users') }}</span>
            </a>
          </li>
        @endif
      @endif
      <li class="menu-header">{{ __('Logout') }}</li>
      <li>
        <a class="nav-link text-danger" href="{{ route('logout') }}"
          onclick="event.preventDefault();
          document.getElementById('logout-form').submit();">
          <i class="fas fa-sign-out-alt"></i> <span>{{ __('Logout') }}</span>
        </a>
      </li>
    </ul>
  </aside>
</div>