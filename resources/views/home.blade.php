@extends('layouts.main_home')
@section('style')
	<style type="text/css">
	.popover {
	    background: #67efbd;
	    white-space: pre-wrap;
	}
	.popover-header{
		font-size: 14px;
		color: black;
	}
	.fc-day-header{
		background: #cdd3d8;
	}
	</style>
@endsection
@section('content')
<div class="row">
	<div class="col-lg-12">	
		@if (session('alert-success'))
			<div class="alert alert-success alert-dismissible show fade">
				<div class="alert-body">
					<button class="close" data-dismiss="alert">
						<span>&times;</span>
					</button>
					{{ session('alert-success') }}
				</div>
			</div>
		@endif

		@if (session('alert-danger'))
			<div class="alert alert-danger alert-dismissible show fade">
				<div class="alert-body">
					<button class="close" data-dismiss="alert">
						<span>&times;</span>
					</button>
					{{ session('alert-danger') }}
				</div>
			</div>
		@endif

	    <div class="card card-primary shadow">
	        <div class="card-header">
	            <h4 class="float-left">Calendar</h4> <a href="{{ route('agenda.create') }}" target="_blank" class="btn btn-primary">{{ __('Tambah Kegiatan') }}</a>
	            <div class="ml-auto">
	            	<a href="http://pdpkk.mitraabadi.info/" class="btn btn-info">{{ __('Home Landing') }}</a>
	            	<a href="{{ route('login') }}"  class="btn btn-success">{{Auth::guard()->check() ? 'Home' : 'Login'}}</a>
	            </div>

	        </div>
	        <div class="card-body">
	            <div id='calendar'></div>
	        </div>
	    </div>
	</div>
	<!-- <div class="col-4">	
	    <div class="card card-primary">
	        <div class="card-header">
	            <h4>Agenda Terdekat</h4>
	        </div>
	        <div class="card-body">
	        	<div class="card card-primary">
			        <div class="card-header">
			            <h4>Agenda 1</h4>
			        </div>
			        <div class="card-body">
			            <h4>Buatin Kotak</h4>
			        </div>
			    </div>
	        </div>
	    </div>
	</div> -->
</div>
@endsection

@section('script')
<script>
    $(document).ready(function() {
        $('#calendar').fullCalendar({
            // put your options and callbacks here
            eventColor : '#fc544b',
            eventTextColor: '#FFF',
            header: {
		    left: 'prev,next',
		    center: 'title',
		    right: 'month'
		   },
		   googleCalendarApiKey: 'AIzaSyDVMhgb62vuES2x4naf-BwXOaTeA4rO9JI',
            events : [
                
                @foreach($tasks as $task) 
                @php
                	$log = $task->log()->orderBy('created_at', 'DESC')->first();
                @endphp
                {
                    title : '{{ $task->name }}',
                    start : '{{ $task->start_time }}',
                    end: '{{ $task->end_time }}',
                    description : `{{ $task->description }}` + `{{ $log ? $log->description . $log->user->name : '' }}`,
                    color: `{{ preg_match("/Pemusik/i", $task->name) ? "#6777ef" : (preg_match("/Pewarta/i", $task->name) ? "#ffa426" : "#63ed7a") }}`
                },
                @endforeach

            ],
            eventSources : [
            	'id.indonesian#holiday@group.v.calendar.google.com'
            ],

            eventClick: function(calEvent, jsEvent, view) {

		        $('#kegiatan').val(calEvent.title);
		        if (calEvent.start) {
		        	$('#start_time').val(moment(calEvent.start).format('YYYY-MM-DD HH:mm:ss'));
		        }
		        if (calEvent.description != '') {
		        	$('.deskripsi').removeClass('d-none');
		        	$('#description').val(calEvent.description);
		        }else{
		        	$('.deskripsi').addClass('d-none');
		        }
		        $('#editModal').modal();
		    },
		    eventRender: function(eventObj, $el, element) {
		    	if (eventObj.description) {
				    $el.popover({
				      title: eventObj.title,
				      content: eventObj.description,
				      trigger: 'hover',
				      placement: 'top',
				      container: 'body',
				    });
		    	}else{
		    		$el.popover({
				      title: eventObj.title,
				      trigger: 'hover',
				      placement: 'top',
				      container: 'body',
				    });
		    	}
		  }
        })
    });
</script>
<div class="modal fade" id="editModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-body">
                <h4>Detail</h4>

                Kegiatan :
                <br />
                <textarea class="form-control" id="kegiatan" name="kegiatan" style="height:120px !important" readonly></textarea>

                Start time :
                <br />
                <input type="text" class="form-control" name="start_time" id="start_time" readonly>

                <div class="deskripsi">	
	                Deskripsi :
	                <br />
	                <textarea class="form-control" id="description" name="description" style="height:100px !important" readonly></textarea>
                </div>

            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
@endsection