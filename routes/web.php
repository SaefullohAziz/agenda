<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('/agenda/create', 'HomeController@create')->name('home.create');
Route::get('/', 'HomeController@index')->name('home');

Route::group(['middleware' => ['auth']], function () {

    Route::post('/list', 'UserController@list')->name('user.account.list');
    Route::resource('user', 'UserController')->except(['destroy']);

    Route::resource('agenda', 'AgendaController')->except(['destroy']);
    Route::prefix('agenda')->name('agenda.')->group( function () {
        Route::post('/list', 'AgendaController@list')->name('list');
    });
});
